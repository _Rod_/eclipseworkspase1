package ReflectionTest;

import java.io.File;
import java.lang.reflect.Modifier;
import java.util.Scanner;

public class ReflectionTests {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Scanner sc = new Scanner(System.in);
		// Class<?> classTwo = Scanner.class;
		// Class<?> classOne = sc.getClass();
		// Class<?> classThree = null;
		// try {
		// classThree = Class.forName("java.util.Scanner");
		// } catch (ClassNotFoundException e) {
		// e.printStackTrace();
		// }
		// System.out.println(classOne);
		// System.out.println(classTwo);
		// System.out.println(classThree);
		// sc.close();
		//
		// HandleError rt = new HandleError();
		// System.out.println(rt.getClass());

//		File file = new File("a.txt");
//		Class<?> fileClass = file.getClass();
//		Class<?> superFileClass = fileClass.getSuperclass();
//		System.out.println(superFileClass.getName());
//		Class<?>[] implementsInterface = fileClass.getInterfaces();
//		for (Class<?> inter : implementsInterface) {
//			System.out.println(inter.toString());

			File file = new File("a.txt");
			Class<?> fileClass = file.getClass();
			int mod = fileClass.getModifiers();
			System.out.println(Integer.toBinaryString(mod));
			System.out.println("Public class " + Modifier.isPublic(mod));
			System.out.println("Private class " + Modifier.isPrivate(mod));
			System.out.println("Abstarct class " + Modifier.isAbstract(mod));
		}
	}


