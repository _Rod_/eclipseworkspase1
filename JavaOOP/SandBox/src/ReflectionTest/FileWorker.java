package ReflectionTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileWorker {
	public String fileName;
	protected long size;
	private boolean isByteFile = true;
	private File file;

	public FileWorker() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FileWorker(String fileName) {
		super();
		this.fileName = fileName;
		this.size = getFileSize(fileName);
	}

	private final long getFileSize(String fileName) {
		// TODO Auto-generated method stub
		this.file = new File(fileName);
		return file.length();
	}

	public void setByteFile(boolean isByteFile) {
		this.isByteFile = isByteFile;
	}

	public byte[] getByteFromFile() {
		if (!isByteFile) {
			throw new IllegalArgumentException("The Symbolic ile");
		}
		byte[] byteArray = new byte[(int) this.file.length()];
		try (FileInputStream fis = new FileInputStream(this.file)){
			fis.read(byteArray);
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return byteArray;
	}
	
	public static String getModifierName (int modifier) {
		String modifierToString = " ";
		
		if (modifier == 2048) {
			modifierToString = "stricftp";
		} else if (modifier == 1024) {
			modifierToString = "abstract";
		} else if (modifier == 512) {
			modifierToString = "interface";
		} else if (modifier == 256) {
			modifierToString = "native";
		} else if (modifier == 128) {
			modifierToString = "transient";
		} else if (modifier == 64) {
			modifierToString = "volatile";
		} else if (modifier == 32) {
			modifierToString = "synchronyzed";
		} else if (modifier == 16) {
			modifierToString = "final";
		} else if (modifier == 8) {
			modifierToString = "static";
		} else if (modifier == 4) {
			modifierToString = "protected";
		} else if (modifier == 2) {
			modifierToString = "private";
		} else if (modifier == 1) {
			modifierToString = "public";
		}
		return "Acess Modifier:" + modifierToString + "\n";									
	}

}
