package Sorting;

public class Element implements Comparable<Element> {
	private int id;

	public Element(int id) {
		super();
		this.id = id;
	}

	public Element() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(Element element) {
		int res = 0;

		if (this.id > element.getId()) {
			res = 1;
		}
		if (this.id < element.getId()) {
			res = -1;
		}
		return res;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	
	
}
