
interface MyLinkedList {
	
	public MyLinkedList MyList();
    public void push_back();
    public void print();
    public Boolean find(int value);

}
