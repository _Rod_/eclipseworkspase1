import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/**
		 * 1) Дан массив {0,5,2,4,7,1,3,19} — написать программу для подсчета нечетных
		 * цифр в нем.
		 */

//		int[] arr = { 0, 5, 2, 4, 7, 1, 3, 19 };
//
//		int count = 0;
//
//		for (int j = 0; j < arr.length; j++) {
//			if (arr[j] % 2 == 0 && arr[j] != 0) {
//				count++;
//			}
//		}
//
//		System.out.println(count);

		//////////////////////////////////////////////////////////////////////////////

		/**
		 * 2) Написать код для возможности создания массива целых чисел (размер
		 * вводиться с клавиатуры) и возможности заполнения каждого его элемента
		 * вручную. Выведите этот массив на экран.
		 */

//		Scanner sc = new Scanner(System.in);
//		System.out.println("Input array size");
//		int n = sc.nextInt();
//		if (n <= 0) {
//			System.out.println("Array size value must be positive");
//		}
//		int[] CustomArray = new int[n];
//
//		int elem;
//
//		for (int i = 0; i < CustomArray.length; i++) {
//			System.out.println("Input element #" + (i + 1));
//			elem = sc.nextInt();
//			CustomArray[i] = elem;
//		}
//
//		sc.close();
//		System.out.println(Arrays.toString(CustomArray));
		
		////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * 3) Создать массив случайных чисел (размером 15 элементов). Создайте второй массив в два раза больше,
		 * первые 15 элементов должны быть равны элементам первого массива, а остальные элементы заполнить удвоенных значением начальных.
		 *  Например
					Было → {1,4,7,2}
					Стало → {1,4,7,2,2,8,14,4}
		 */
		
//		int [] arr1 = new int[15];
//		
//		for (int i = 0; i < arr1.length; i++) {
//			arr1[i] = (int) (Math.random() * 100);
//		}
//				
//		int [] arr2 = Arrays.copyOf(arr1, arr1.length * 2);
//		System.arraycopy(arr1, 0, arr2, arr1.length, arr1.length);
//
//		for (int i = arr1.length; i < arr2.length; i++) {
//			arr2[i] *=2; 
//		}
//		
//		System.out.println(Arrays.toString(arr1));
//		System.out.println(Arrays.toString(arr2));
		
		/**
		 * 4) Введите строку текста с клавиатуры — реализуйте программу для возможности подсчета количества символа — 'b' в этой строке,
		 * с выводом результат на экран.
		 */

		Scanner sc2 = new Scanner(System.in);
		System.out.println("Input text");
		String line = sc2.nextLine();
		int count = 0;
		
		for (int i = 0; i < line.length(); i++) {
			char symbol = line.charAt(i);
			if(symbol == 'b' || symbol == 'B') {
				count = count + 1;
			}
		}
		sc2.close();
		System.out.println("There are " + count + " 'b'" + " letters in requested string");
		
//		char[] text  = line.toCharArray();
//		
//		for (int i = 0; i < textToChar.length; i++) {
//			if(text[i] == 'b' || text[i] == 'B') {
//				count++;
//			}
//		}
//		sc2.close();
//		System.out.println("There are " + count + " 'b'" + " letters in requested string");
	}
}
