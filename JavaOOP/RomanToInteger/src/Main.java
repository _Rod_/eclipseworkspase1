import java.util.HashMap;
import java.util.Map;

public class Main {

	public static int romanToInt(String s) {

		Map<Character, Integer> romanMap = new HashMap<>();
		romanMap.put('I', 1);
		romanMap.put('V', 5);
		romanMap.put('X', 10);
		romanMap.put('L', 50);
		romanMap.put('C', 100);
		romanMap.put('D', 500);
		romanMap.put('M', 1000);

		int result = 0;
		int i = 0;
		int j = 0;
		 for (i = 0, j = i + 1; j < s.length(); ) {
			
				if (romanMap.get(s.charAt(i)) < romanMap.get(s.charAt(j))) {
					result += romanMap.get(s.charAt(j)) - romanMap.get(s.charAt(i));
					i+=2;
					j+=2;
				}

				if (romanMap.get(s.charAt(i)) >= romanMap.get(s.charAt(j)) && i != j) {
					result += romanMap.get(s.charAt(i));
					i++;
					j++;
				}

		
		}

		if (i < s.length()) {
			result += romanMap.get(s.charAt(i));
		}
		return result;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(romanToInt("XIV"));

//	  System.out.println(Converter.romanToInt("XIX"));
	}

}
