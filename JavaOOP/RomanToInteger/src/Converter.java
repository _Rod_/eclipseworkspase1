import java.util.HashMap;
import java.util.Map;

public class Converter {
	public static int romanToInt(String s) {
		
		   Map<Character, Integer> romanMap = new HashMap<>();
	        romanMap.put('I',1);
	        romanMap.put('V',5);
	        romanMap.put('X',10);
	        romanMap.put('L',50);
	        romanMap.put('C',100);
	        romanMap.put('D',500);
	        romanMap.put('M',1000);

		int result = 0;

		for (int i = 0, j = i + 1; i < s.length(); i++) {
		
			if (romanMap.get(s.charAt(i)) < romanMap.get(s.charAt(j))) {
				result += romanMap.get(s.charAt(j)) - romanMap.get(s.charAt(i));
			}

			if (romanMap.get(s.charAt(i)) > romanMap.get(s.charAt(j))) {
				result += romanMap.get(s.charAt(j)) + romanMap.get(s.charAt(i));
			}
		}
		return result;
	}
}
